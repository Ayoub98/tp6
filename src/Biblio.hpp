#ifndef BIBLIO_HPP_
#define BIBLIO_HPP_
#include "Livre.hpp"
#include <string>
#include <vector>

class Biblio: public std::vector<Livre>{
public:
    void afficher()const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void ecrireFichier(const std::string & nomFichier) const;
    void LireFichier(const std::string & nomFichier);

};

#endif // BIBLIOTHEQUE_HPP
