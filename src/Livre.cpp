#include "Livre.hpp"
#include <iostream>
Livre::Livre(const std::string &titre,const std::string &auteur,int annee):_titre(titre),_auteur(auteur),_annee(annee){
	if(_titre.find(";")!=std::string::npos)
		{throw std::string("erreur titre ");
			}
		if(_auteur.find(";")!=std::string::npos)
		{throw std::string("erreur auteur");
			}	
		if(_titre.find("\n")!=std::string::npos){throw std::string("erreur retour a la ligne titre");}
		if(_auteur.find("\n")!=std::string::npos){throw std::string("erreur retour a la ligne auteur");}
}
const std::string &Livre::getAuteur()const { return _auteur;}
const std::string &Livre::getTitre() const {return _titre;}
int Livre::getAnnee() const  { return _annee;}

bool Livre::operator<(const Livre &l2) const {
	if(_auteur<l2._auteur) return true;
		else return _auteur==l2._auteur && _titre < l2._titre;			
}

bool operator==(const Livre & l1, const Livre & l2)
{
	return l1.getTitre() == l2.getTitre() and l1.getAuteur() == l2.getAuteur() and l1.getAnnee() == l2.getAnnee();
}
std::ostream & operator <<(std::ostream &os,const Livre &l){
	os<<l.getTitre()<<";"<<l.getAuteur()<<";"<<l.getAnnee();
	return os;}
	
std::istream & operator >>(std::istream &is,const Livre &l){
	std::getline(is,l.getAuteur(),";");
	std::getline(is,l.getAnnee(),";");
	std::getline(is,l.getTitre(),";");
	return is;
	}



